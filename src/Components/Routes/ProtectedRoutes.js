import {Route, Redirect} from "react-router-dom"
import { useSelector } from 'react-redux'


/**
 * Wraps routing information to prevent user from navigating to protected pages when not logged in
 * @param {Component} param0 
 * @returns A react route element with additional logic
 */
export function ProtectedRouteSignIn({ component: Component, ...restOfProps }) {
    const loggedIn = useSelector(state => state.userData.loggedIn)
    return (
      <Route
        {...restOfProps}
        render={(props) =>
          loggedIn ? <Component {...props} /> : <Redirect to="/" />
        }
      />
    );
  }
  /**
 * Wraps routing information to prevent user from navigating back to login page when logged in
 * @param {Component} param0 
 * @returns A react route element with additional logic
 */
  export function ProtectedRouteAuthenticated({ component: Component, ...restOfProps}) {
    const loggedIn = useSelector(state => state.userData.loggedIn)
    return (
      <Route
        {...restOfProps}
        render={(props) =>
          loggedIn ? <Redirect to="/translation"/> : <Component {...props} /> 
        }
      />
    );
  }