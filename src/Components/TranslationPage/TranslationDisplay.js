import {getLetter} from "../../assets/Letters"

/**
 * Element that displays the text entered as images of signs in sign language
 * @param {String} props The string that is displayed
 * @returns A react compenent containing images
 */
export default function TranslationDisplay(props) {

    const translationText = props.currentTranslation

    return (
        <div className="col-7 offset-2 p-2">
            {translationText && [...translationText].map((char,key) => getLetter(char.toLowerCase()) ? 
            <span key={key}>
                <img className="img-thumbnail img-fluid m-2" style={{width: "10%"}} src={`${getLetter(char.toLowerCase())}`} alt="sign"/>
            </span> 
            : <span><div></div></span> )}
        </div>
    )
}