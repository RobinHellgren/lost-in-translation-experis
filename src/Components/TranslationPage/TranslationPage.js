import { useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { setCurrentTranslation } from "../../Store/Actions/translationActions";
import { saveActionCurrrentTranslation } from "../../Store/Actions/userActions";
import Wrapper from "../ProfilePage/TranslationWrapper";

/**
 * React page that allows the user to translate text into sign language
 * @returns A react component
 */
export default function TranslationPage() {
    
    const currentTranslation = useSelector(state => state.translationData.currentTranslation.translationText)
    const user = useSelector(state => state.userData.user)
    const dispatch = useDispatch()
    const [translationText, setTranslationText] = useState({ 
        translationText: ""
    })
    /**
     * Updates the state of the component to reflect the text in the input field
     * @param {onChangeEvent} event Event triggered every time the text in the input changes
     */
    function inputChanged(event) {
        setTranslationText({
            ...translationText,
            [event.target.id]: event.target.value
        })
    }
    /**
     * Sets the current translation in redux and saves it to the database
     * @param {SubmitEvent} event Event triggered when the user presses the show translation button
     */
    const showTranslation = (event) => {
        event.preventDefault()
        dispatch(setCurrentTranslation(translationText))
        user.translations.push(translationText.translationText)
        dispatch(saveActionCurrrentTranslation(user.id, user))
    }
    return (
        <div>
            <form onSubmit={showTranslation} className="col-6 offset-3">
                <input type="text" id="translationText" onChange={inputChanged} className="form-control mt-4 text-center" placeholder="Enter text to translate"></input>
                <div className="offset-5">
                    <button className="btn btn-success my-3 "  type="submit">Translate</button>
                </div>
            </form>
            <div >
                 {currentTranslation ==="" ? <div></div> : <Wrapper currentTranslation={currentTranslation}/>} 
            </div>
        </div>
    )
}