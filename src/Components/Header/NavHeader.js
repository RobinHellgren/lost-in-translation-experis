
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import logo from '../../assets/Logo.png'
import { userActionLogout } from '../../Store/Actions/userActions'
/**
 * Navbar React component displayed at top of page
 * @returns Navbar as React component
 */
export default function NavHeader() {
    
    const user = useSelector(state => state.userData.user)
    const loggedIn = useSelector(state => state.userData.loggedIn)
    const dispatch = useDispatch()

    /**
     * Triggered when the logout button is clicked, resets the application state back to initial and clears localstorage
     */
    const logout = () => {
        localStorage.removeItem("user")
        dispatch(userActionLogout())
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <ul className="navbar-nav me-auto">
                    <li className="nav-item mx-3">
                    <Link to="/"><img src={logo} width="42px" height="42px" alt="logo"/></Link>
                    </li>
                    <li className="nav-item mt-2">
                    <h5>Lost in translation app</h5>
                    </li>
                </ul>
                <Link className="navbar-text" style={{ textDecoration: 'none', color: "red", fontSize: 24}} to="/profile">{loggedIn && user.name}</Link>
                <button className="btn-sm btn-danger mx-3" onClick={logout}> Logout</button>
            </div>
        </nav>
    )
}