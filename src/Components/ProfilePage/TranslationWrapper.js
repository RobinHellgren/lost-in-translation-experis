import TranslationDisplay from "../TranslationPage/TranslationDisplay";
/**
 * Wraps a TranslationDisplay object to display a title above the sign language images
 * @param {string} props string containing translation to display
 * @returns React component wrapping a TranslationDisplay element
 */
export default function TranslationWrapper(props) {

    return (
        <div>
            <h5 className="col-4 offset-2 mt-5">{props.currentTranslation}</h5>
            <TranslationDisplay currentTranslation={props.currentTranslation}/>
        </div>
        
    )
}