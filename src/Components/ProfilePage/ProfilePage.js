import { useSelector } from 'react-redux'
import TranslationWrapper from "./TranslationWrapper"
import { saveActionCurrrentTranslation } from "../../Store/Actions/userActions";
import { useDispatch } from "react-redux";
import { setCurrentTranslation } from '../../Store/Actions/translationActions';
import { useHistory } from 'react-router';
/**
 * Users profile page showing saved translations
 * @returns Users profile page as a React component
 */
export default function ProfilePage() {
    const history = useHistory()
    const dispatch = useDispatch()
    const user = useSelector(state => state.userData.user)
    const translations = user.translations
    /**
     * Clears all saved translations for the user, the currently active translation and refreshes the page
     */
    function clearTranslations(){
        user.translations = []
        dispatch(saveActionCurrrentTranslation(user.id, user))
        dispatch(setCurrentTranslation(""))
        history.push("/profile")
    }
    
    return (
        <div className="m-5">
            <h3>{}</h3>
            <div>
                <h3 className="offset-3">Welcome {user.name}, your last translations:</h3>
            </div>
            <div>
                <div>
                    <h3>{translations.map(translation => 
                            <TranslationWrapper currentTranslation={translation}/>
                    )}</h3>
                </div>
                 <button className="btn-danger col-3 offset-4" onClick={clearTranslations}>Clear translations</button>

            </div>
        </div>
    )
}
