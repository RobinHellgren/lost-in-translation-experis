import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { userActionAttempt } from "../../Store/Actions/userActions";
/**
 * Login page that lets the user log in with their name
 * @returns Login page as react component
 */
export default function StartPage () {

    const history = useHistory()
    const dispatch = useDispatch()
    const attemptingLogin = useSelector(state => state.userData.attemptingLogin)

    useEffect(() => {
        let savedUser = localStorage.getItem("user")
        if(savedUser !== undefined  && savedUser !== null && !attemptingLogin ) {
            dispatch(userActionAttempt({name: savedUser}))
            history.push("/translation")
        }
    }, [])

    const [credentials, setCredentials] = useState({
        name: ""
    })

    /**
     * Updates the state of the component to reflect the text present in the text input field
     * @param {onChangeEvent} event event for each time the text in the input field changes
     */
    function inputChanged(event) {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })
    }
    /**
     * Dispatches a login action to redux, saves the name of the user that logged in to local storage and navigates to the translation page
     * @param {SubmitEvent} event Event that gets triggered when user presses the login button
     */
    function enterTranslationPage(event) {
        event.preventDefault()
        dispatch(userActionAttempt(credentials))
        localStorage.setItem("user", credentials.name)
        history.push("/translation")
    }

    return (
        <div>
            <form onSubmit={enterTranslationPage}>
                <input type="text" id="name" onChange={inputChanged} className="form-control" placeholder="Enter name"></input>
                <button type="submit" className="btn btn-success mt-3">Login</button>
            </form>
        </div>
    );
}

