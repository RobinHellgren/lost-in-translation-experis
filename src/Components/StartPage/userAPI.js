
const SERVER_URL = "https://lost-in-translation-server.herokuapp.com"
/**
 * Gets all users from the API server
 * @returns All users from the API server
 */
export function getUsers() {
    return fetch(SERVER_URL + '/users', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(response => response)
}
/**
 * Adds a user with the specified name to the API server
 * @param {Name} name Name of the new user
 * @returns The user that got added to the API server
 */
export function addUser(name) {
    return fetch( SERVER_URL + '/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "name": name,
            "translations": []
        })
    })
    .then(response => response.json())
    .then(response => response)
}
/**
 * Updates the specified user object to include new translations
 * @param {UserID} id Id of the updated user
 * @param {UserObject} user New version of the user object
 * @returns The updated version of the User object
 */
export function saveTranslation(id, user) {
    return fetch(`${SERVER_URL}/users/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "id": user.id,
            "name": user.name,
            "translations": user.translations
        })
    })
    .then(response => response.json())
    .then(response => response)
}
