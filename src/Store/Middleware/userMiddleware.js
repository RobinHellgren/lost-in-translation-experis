import { userActionFailed, userActionSetUser, USER_ACTION_ATTEMPT,
    TRANSLATION_ACTION_SAVE, saveActionFailed} from "../Actions/userActions";
import { getUsers, addUser, saveTranslation } from "../../Components/StartPage/userAPI";

export const userMiddleware = ({dispatch}) => next => action => {
    next(action)
    switch (action.type) {
        case USER_ACTION_ATTEMPT:
            getUsers()
            .then(response => {
                response = response.filter(user => user.name === action.payload.name)
                if(response.length){
                    dispatch(userActionSetUser(response[0]))
                }
                else {
                    addUser(action.payload.name)
                    .then(response => {
                        dispatch(userActionSetUser(response))
                    })
                    .catch(error => {
                        console.error(error)
                        dispatch(userActionFailed(error))
                    })
                }
            })
            .catch(error => {
                console.error(error)
                dispatch(userActionFailed(error))
            })
            break;
        case TRANSLATION_ACTION_SAVE:
            saveTranslation(action.id, action.payload)
            .then(response => response)
            .catch(error => {
                error(error)
                dispatch(saveActionFailed(error))
            })
            break;
        default:
            break;
    }
}