export const USER_ACTION_ATTEMPT = "[user] ATTEMPTING"
export const USER_ACTION_ERROR = "[user] ERROR"
export const USER_ACTION_LOGOUT = "[user] LOGOUT"
export const USER_ACTION_SET_USER = "[user] SET USER"
export const TRANSLATION_ACTION_SAVE = "[user] SAVE-CURRENT-TRANSLATION"
export const TRANSLATION_ACTION_SAVE_ERROR = "[user] SAVE-CURRENT-ERROR"

/**
 * Action for user login
 * @param {string} name Name of user attemptin to login 
 * @returns action object
 */
export const userActionAttempt = name => ({
    type: USER_ACTION_ATTEMPT,
    payload: name
})
/**
 * Action to set user 
 * @param {object} user 
 * @returns action object
 */
export const userActionSetUser = user => ({
    type: USER_ACTION_SET_USER,
    payload: user
})
/**
 * Action for errorhandling
 * @param {string} errormsg error message
 * @returns action object
 */
export const userActionFailed = errormsg => ({
    type: USER_ACTION_ERROR,
    payload: errormsg
})
/**
 * action to logout user
 * @returns action object
 */
export const userActionLogout = () => ({
    type: USER_ACTION_LOGOUT
})
/**
 * Action to save current translation
 * @param {number} id user id
 * @param {object} user user to save translation to
 * @returns 
 */
export const saveActionCurrrentTranslation = (id, user) => ({
    type: TRANSLATION_ACTION_SAVE,
    id: id,
    payload: user
})
/**
 * Action for handling errors when saving to the API server
 * @param {string} errormsg error message
 * @returns action object
 */
export const saveActionFailed = errormsg => ({
    type: TRANSLATION_ACTION_SAVE_ERROR,
    payload: errormsg
})