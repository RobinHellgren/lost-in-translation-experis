export const TRANSLATION_SET_CURRENT = "[translation] SET-CURRENT"

/**
 * Set current translation
 * @param {string} text Translation to set to state
 * @returns action object
 */
export const setCurrentTranslation = (text) => ({
    type: TRANSLATION_SET_CURRENT,
    payload: text
})
