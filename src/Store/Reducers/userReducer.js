import { USER_ACTION_ATTEMPT, USER_ACTION_SET_USER, USER_ACTION_ERROR, USER_ACTION_LOGOUT, TRANSLATION_ACTION_SAVE } from "../Actions/userActions";

const initialState = {
    attemptingLogin: false,
    loggedIn: false,
    user: {
        name:"",
        translations: []
    },
    errormsg: ""
}

/**
 * Reduce user actions
 * @param {object} state current state
 * @param {object} action action to trigger
 * @returns new or current state
 */
export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_ACTION_ATTEMPT:
            return {
                ...state,
                attemptingLogin: true
            };
        case USER_ACTION_ERROR:
            return {
                ...state,
                attemptingLogin: false,
                errormsg: action.payload
            }
        case USER_ACTION_SET_USER:
            return {
                ...initialState,
                loggedIn: true,
                user: action.payload
            }
        case USER_ACTION_LOGOUT:
            return {
                initialState
            }
        case TRANSLATION_ACTION_SAVE:
            return {
                ...state,
                user: action.payload
            }
        default:
            return state;
    }
}