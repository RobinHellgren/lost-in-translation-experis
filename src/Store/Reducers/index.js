import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import {translationReducer} from "./translationReducer"

const appReducer = combineReducers({
    userData: userReducer,
    translationData: translationReducer,
})

export default appReducer;