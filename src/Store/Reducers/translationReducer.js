import { TRANSLATION_SET_CURRENT } from "../Actions/translationActions";

const initialState = {
        currentTranslation: ""
}

/**
 * Reduce the current translation
 * @param {object} state current state
 * @param {object} action action object
 * @returns new state or current state
 */
export const translationReducer = (state = initialState, action) => {
    switch (action.type) {
        case TRANSLATION_SET_CURRENT:
            return {
                ...state,
                currentTranslation: action.payload
            };
        default:
            return state;
    }
}
 