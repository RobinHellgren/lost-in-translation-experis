import './App.css';
import './Components/StartPage/StartPage.js'
import NavHeader from'./Components/Header/NavHeader'
import {BrowserRouter as Router, Switch} from "react-router-dom";
import StartPage from './Components/StartPage/StartPage.js';
import TranslationPage from './Components/TranslationPage/TranslationPage';
import ProfilePage from './Components/ProfilePage/ProfilePage';
import {ProtectedRouteAuthenticated, ProtectedRouteSignIn} from './Components/Routes/ProtectedRoutes'

function App() {
  return (
    <div className="container">
      
        <Router>
          <NavHeader/>
          <Switch>
          <ProtectedRouteAuthenticated
              exact 
              path="/" 
              component={StartPage}
            />
            <ProtectedRouteSignIn
              path="/translation"
              component={TranslationPage}
              />
            <ProtectedRouteSignIn
              path="/profile"
              component={ProfilePage}
              />
          </Switch>
        </Router>
    </div>
  );
}

export default App;
